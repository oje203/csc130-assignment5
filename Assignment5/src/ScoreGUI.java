import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class ScoreGUI extends JFrame implements ActionListener{
	final int SIZE=5;
	JButton jbtnCalculate = new JButton ("Calculate");
	/**
	 *Initialize text fields of score and weight
	 * **/
	JTextField [] jtxtScore = new JTextField[SIZE];
	JTextField [] jtxtWeight = new JTextField[SIZE];
	//Add labels
	JLabel [] jlblModuleName = new JLabel[SIZE];
	JPanel scorePanel = new JPanel();
	JPanel buttonPanel = new JPanel();
	/**
	 * declares arrays for scores and weights
	 * used for ScoreCalculator
	 * **/
	double []scores = new double[SIZE]; 
	double []weights = new double[SIZE]; 
	
/**
*Construct the GUI for score calculator
*/
	public ScoreGUI(){
		/**
		 *  Name each label
		 */
		jlblModuleName [0]= new JLabel("Assignments");
		jlblModuleName [1] = new JLabel("Mid term");
		jlblModuleName [2]= new JLabel("Final Exam");
		jlblModuleName [3]= new JLabel("Final Project");
		jlblModuleName [4]= new JLabel("Grade Average");
		/**
		 *  Set color and Layout
		 */
		scorePanel.setBackground(Color.gray);
		scorePanel.setLayout(new GridLayout(7,3,5,9));
	for(int i=0; i<SIZE;i++){
		/**
		 * add JLabels to panel
		 */
		scorePanel.add(jlblModuleName[i]);
		/**
		 * determine size of text fields
		 */
		jtxtScore[i] = new JTextField(8);
		/**
		 * add text field to panel
		 */
		scorePanel.add(jtxtScore[i]);
		jtxtWeight[i] = new JTextField(8);
		scorePanel.add(jtxtWeight[i]);
		/**
		 * add(jtxtWeight[i]);
		 */
		}
		buttonPanel.setBackground(Color.CYAN);
		jbtnCalculate.addActionListener(this);
		buttonPanel.add(jbtnCalculate);
		/**
		 * set location of panel
		 */
		add(scorePanel, BorderLayout.CENTER);
		add(buttonPanel, BorderLayout.SOUTH);
		/**
		 * set values of frame
		 * */
		setVisible(true);
		setSize(500,500);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
	}
	/**
	 * calculate grade average scores
	 * @param score an array of scores
	 * @param weight an array of weights
	 * @param grade average score
	 * @return total grade
	 * */
	public double calculateScore(double[]score, double[]weight){
		
	//Multiply each JtextField from 0 to 3
		double grade=0.0;
		
	grade = (score[0]*weight[0]+score[1]*weight[1]+score[2]*weight[2]+score[3]*weight[3]);
	
	
	return grade;	
	}
	/**
	 * Get data stored in JTexts to calculate Score
	 * @param get scores and weights
	 * @param the total score in [4]
	 * */
	 
	public void actionPerformed(ActionEvent e){
		for(int i=0;i<4;i++){
		scores[i] = Double.parseDouble(jtxtScore[i].getText());
		weights[i] = Double.parseDouble(jtxtWeight[i].getText());
		}
		/**
		 * store the result in JtxtScore[4]
		 */
		jtxtScore[4].setText(calculateScore(scores,weights)+"");
	
	}	
}
